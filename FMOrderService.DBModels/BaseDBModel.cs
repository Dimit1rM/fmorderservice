﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;

namespace FMOrderService.DBModels
{
    [DataContract]
    public class BaseDBModel : IBaseDBModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public virtual ObjectId ID { get; set; }
        public BsonDateTime DateCreated { get; set; }
        public BsonDateTime DateUpdated { get; set; }


    }
}