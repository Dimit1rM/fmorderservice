﻿using MongoDB.Bson;

namespace FMOrderService.DBModels
{
    public interface IBaseDBModel
    {
        ObjectId ID { get; set; }
    }
}