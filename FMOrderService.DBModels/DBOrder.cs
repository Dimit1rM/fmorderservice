﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.DBModels
{
    [DataContract]
    public class DBOrder : BaseDBModel
    {
        [DataMember]
        public List<WorkflowLog> WorkflowLogs { get; set; }

        public DBOrder()
        {
            this.WorkflowLogs = new List<WorkflowLog>();
        }

        public string GetOrderIDAsString()
        {
            return this.ID != ObjectId.Empty ? this.ID.ToString() : string.Empty;
        }
        public void Log(int status, string text)
        {
            var workflowLog = new WorkflowLog() { Status = status, Text = text, Date = DateTime.UtcNow };
            this.WorkflowLogs.Add(workflowLog);
        }

        [DataMember]
        public int OrderNumber { get; set; }
        [DataMember]
        public int OrderStatus { get; set; }
        [DataMember]
        public int OrderType { get; set; }
        [DataMember]
        public double OrderTotal { get; set; }

        [DataMember]
        public DateTime OrderTimeUTC { get; set; }

        [DataMember]
        public DateTime CreatedTimeUTC { get; set; }
        
        [DataMember]
        public DateTime KitchenTimeUTC { get; set; }
        
        [DataMember]
        public DateTime? DriverTimeUTC { get; set; }

        [DataMember]
        public int MerchantID { get; set; }
        [DataMember]
        public string MerchantUsername { get; set; }
        [DataMember]
        public int RestaurantID { get; set; }
        [DataMember]
        public string RestaurantName { get; set; }
        [DataMember]
        public string RestaurantAddress { get; set; }

        [DataMember]
        public double Discount { get; set; }

        [DataMember]
        public double DiscountPercent { get; set; }
        [DataMember]
        public CouponModel CouponData { get; set; }

        //[DataMember]
        //public long? OriginalOrderID { get; set; }

        [DataMember]
        public int LocationID { get; set; }//office orders

        [DataMember]
        public List<ItemModel> Items { get; set; }

        [DataMember]
        public ContactInformationModel ContactInformation { get; set; }

        [DataMember]
        public DeliveryInformationModel DeliveryInformation { get; set; }

        [DataMember]
        public DeviceInformationModel DeviceInformation { get; set; }
      
        [DataMember]
        public string Seats { get; set; }

        [DataMember]
        public string Comments { get; set; }
        
        [DataMember]
        public int? RejectOrderReasonID { get; set; }
        [DataMember]
        public string RejectOrderReason { get; set; }

        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public int CultureID { get; set; }
        [DataMember]
        public string CustomerUsername { get; set; }
        [DataMember]
        public string OperatorUsername { get; set; }

        [DataMember]
        public bool AnalyticsFlag { get; set; }
        //StartWokringTime - за да не се спами преди работа
        //AutoAcceptedOn - кога да се приеме автоматично поръчката - след края на часа, деня и т.н.
        [DataMember]
        public InvoiceDataModel InvoiceData { get; set; }

        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public int PaymentType { get; set; }

        [DataMember]
        public int? QSSellID { get; set; }

        //[DataMember]
        //public int? LogisticPartnerID { get; set; }
        //[DataMember]
        //public int? DeliveryBundleID { get; set; }

        //metadata from api SMTP, POS type and credientials, EMAILs
        //and remoteIDs and titles

    }

    [DataContract]
    public class ItemModel : BaseDBModel
    {
        [DataMember]
        public int PriceItemID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PriceItemName { get; set; }
        [DataMember]
        public string RemoteID { get; set; }
        [DataMember]
        public int CategoryID { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public bool PrintItemsSplited { get; set; }
        [DataMember]
        public int MenuID { get; set; }
        [DataMember]
        public string MenuName { get; set; }
        [DataMember]
        public List<ModificatorModel> Modificators { get; set; }

        //public List<IngredientModel> Ingredients { get; set; }
        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public decimal? PromoPrice { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal? EndPrice { get; set; }

        public ItemModel()
        {
            Modificators = new List<ModificatorModel>();
        }
    }

    [DataContract]
    public class ModificatorModel
    {
        [DataMember]
        public int ModificatorID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string RemoteID { get; set; }
        [DataMember]
        public int OrderNumber { get; set; }
        [DataMember]
        public int GroupID { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public bool DelimiterGroup { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal? EndPrice { get; set; }
    }

    [DataContract]
    public class CouponModel
    {
        [DataMember]
        public string CouponCode { get; set; }
        [DataMember]
        public int CouponType { get; set; }
        [DataMember]
        public double CouponValue { get; set; }
        [DataMember]
        public double RealCouponValue { get; set; }
    }

    [DataContract]
    public class InvoiceDataModel
    {
        [DataMember]
        public bool IsCompany { get; set; }
        [DataMember]
        public string InvoiceAddress { get; set; }
        [DataMember]
        public string InvoiceMOL { get; set; }
        [DataMember]
        public string InvoiceBulstat { get; set; }
        [DataMember]
        public string InvoiceCity { get; set; }
        [DataMember]
        public string InvoiceEGN { get; set; }
        [DataMember]
        public string InvoiceName { get; set; }
    }

    [DataContract]
    public class DeliveryInformationModel
    {
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int? AddressID { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public int FeeID { get; set; }
        [DataMember]
        public double Fee { get; set; }
        [DataMember]
        public string DriverCode { get; set; }
        [DataMember]
        public double DriverDistance { get; set; }
        [DataMember]
        public double DriverDuration { get; set; }

    }

    [DataContract]
    public class ContactInformationModel
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Email { get; set; }
    }

    [DataContract]
    public class DeviceInformationModel
    {
        [DataMember]
        public string OS { get; set; }
        [DataMember]
        public string DeviceID { get; set; }
        [DataMember]
        public string Package { get; set; }
    }

    [DataContract]
    public class WorkflowLog
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }

    public class DBOrderStatus
    {
        public static readonly int FailedToValidate = 1;
        public static readonly int Pending = 2;
        public static readonly int SavedToDatabase = 3;
        public static readonly int AddedToDatabase = 4;


        public static string GetStatusText(int status)
        {
            switch (status)
            {
                case 1:
                    return "WorkflowEngine has failed to validate the order!";
                case 2:
                    return "The order is now pending.";
                case 3:
                    return "The order was saved successfully!";
                case 4:
                    return "The order was created successfully!";
                default:
                    return "Order's status is unkown!";
            }
        }
    }
}
