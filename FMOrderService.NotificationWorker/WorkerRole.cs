﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using FMOrderService.WorkflowEngine.ServiceBusModels;
using FMOrderService.WorkflowEngine.Models.Steps;

namespace FMOrderService.NotificationWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        // The name of your queue
        // The name of your queue
        const string topicPath = "orderservicemain";
        //const string connectionString = "Endpoint=sb://devorderservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManagePolicy;SharedAccessKey=fwCI1RUN1dwJxgMaO5Ah4lZT0vAfOw3mILB/cNI1CgQ=;EntityPath=orderservicemain";
        const string connectionString = "Endpoint=sb://devorderservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=pTBCwqpnZ5M4DQSoEb5E3bGGqjQ+QjfKfmH8v7osH08=";
        const string subscriptionName = "NotificationWorker";

        // QueueClient is thread-safe. Recommended that you cache 
        // rather than recreating it on every request
        SubscriptionClient client; //= SubscriptionClient.CreateFromConnectionString(connectionString, topicPath, subscriptionName);
        ManualResetEvent CompletedEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.WriteLine("Starting processing of messages");

            // Initiates the message pump and callback is invoked for each message that is received, calling close on the client will stop the pump.
            client.OnMessage((receivedMessage) =>
            {
                //var log = new Logger();
                //log.Debug("Notification Worker log");
                try
                {
                    var busMessage = receivedMessage.GetBody<BusMessageModel>();
                    if (busMessage.WorkflowEngine.NeedProcessing(typeof(SendEmailStep)))
                    {
                        // Process the message
                        //Trace.WriteLine("Processing Service Bus message: " + receivedMessage.SequenceNumber.ToString());
                        //Do some work
                        busMessage.WorkflowEngine.LogOrderState("Message has been received");
                        busMessage.WorkflowEngine.SaveOrderToDB();
                        //busMessage.WorkflowEngine.NextStep();
                    }
                    
                }
                catch (Exception ex)
                {
                    Trace.TraceError("FMOrderService.NotificationWorker.WorkerRole.Run[client.OnMessage]: {0}", ex.ToString());
                    // Handle any message processing specific exceptions here
                }
            });

            CompletedEvent.WaitOne();
        }

        public override bool OnStart()
        {
            Trace.WriteLine("OnStart is called!");
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            // Create the queue if it does not exist already
            string connectionString = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString");
            var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);
            if (!namespaceManager.TopicExists(topicPath))
            {
                namespaceManager.CreateTopic(topicPath);
            }

            // Initialize the connection to Service Bus Topic
            try
            {
                client = SubscriptionClient.CreateFromConnectionString(connectionString, topicPath, subscriptionName);
            }
            catch (Exception ex)
            {
                Trace.TraceError("FMOrderService.NotificationWorker.WorkerRole.Start: {0}", ex.ToString());
            }
            return base.OnStart();
        }

        public override void OnStop()
        {
            // Close the connection to Service Bus Queue
            client.Close();
            CompletedEvent.Set();
            base.OnStop();
        }
    }
}
