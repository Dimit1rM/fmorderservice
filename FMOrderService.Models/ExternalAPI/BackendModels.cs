﻿using FMOrderService.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Models.ExternalAPI
{
    public class RestaurantDataRequestModel
    {
        public int CultureID { get; set; }
        public int RestaurantID { get; set; }
    }

    public class RestaurantDataModel
    {
        public int MerchantID { get; set; }
        public string MerchantName { get; set; }

        public int ID { get; set; }
        public string RemoteID { get; set; }

        public string Country { get; set; }
        public string TimeZone { get; set; }

        public int CultureID { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public bool AutoComplete { get; set; }
        public int ApprovedOrderConfiguration { get; set; }
        public int POSType { get; set; }
        public PrinterDataModel PrinterData { get; set; }

        public string MetriloAPIKey { get; set; }

        public RestaurantDataModel()
        {
        }

    }

    public class NotificationsDataModel
    {
        public string MerchantMailContact { get; set; }
        public string MerchantLogo { get; set; }
        public string MerchantMailTo { get; set; }
        public string MerchantMailFrom { get; set; }
        public string MerchantEmail { get; set; }
        public bool NotifyMerchantForMissedOrder { get; set; }
        public SMTPSettingsModel SMTPSettings { get; set; }

        public List<string> RestaurantEmails { get; set; }

        public bool AddOperatorMails { get; set; }
        public List<string> OperatorEmails { get; set; }
        public List<string> OperatorTelephones { get; set; }
        public string Regards { get; set; }
    }

    public class SMTPSettingsModel
    {
        public string SMTPServer { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPPassword { get; set; }
        public bool SMTPHasSLL { get; set; }
        public int SMTPPort { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(SMTPServer) && !string.IsNullOrEmpty(SMTPUsername) &&
                !string.IsNullOrEmpty(SMTPPassword);
        }
    }

    public class POSDataRequestModel
    {
        public int RestaurantID { get; set; }
        public int PaymentType { get; set; }
        public int DeliveryFeeID { get; set; }
    }


    public class POSDataModel
    {

        public int POSType { get; set; }
        public bool SendToPOSAsCompleted { get; set; }
        public string MerchantPOSUrl { get; set; }
        public string MerchantPOSUsername { get; set; }
        public string MerchantPOSPassword { get; set; }
        public string MerchantAPIKey { get; set; }

        public string POSUrl { get; set; }
        public string POSUsername { get; set; }
        public string POSPassword { get; set; }
        public string POSAPIKey { get; set; }

        public string DeliveryFeeRemoteID { get; set; }
        public string PaymentTypeRemoteID { get; set; }
        public string RestaurantRemoteID { get; set; }
    }

    public class PrinterDataModel
    {
        public int PrinterRawSize { get; set; }
        public string PrinterCutCommand { get; set; }
        public string PrinterRingCommand { get; set; }
        public bool AutoAccept { get; set; }
        public int AutoAcceptOffset { get; set; }

        public int PrinterType { get; set; }
        public int PrintOrderType { get; set; }
    }

    public class ItemsDataRequestModel
    {
        public List<ItemModel> PriceItems { get; set; }
        public int CultureID { get; set; }

        public ItemsDataRequestModel()
        {
            PriceItems = new List<ItemModel>();
        }
    }

}
