﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Models.API.RoleModels
{
    public class RoleModels
    {
        public class RolesViewModel
        {
            public List<UserInRole> Users { get; set; }
        }

        public class UserInRole
        {
            public Guid ID { get; set; }
            public string Name { get; set; }
            public List<UserRoleViewModel> Roles { get; set; }
        }

        public class UserRoleViewModel
        {
            public Guid ID { get; set; }
            public string Name { get; set; }
        }

        //public class CreateRoleBindingModel
        //{
        //    [Required]
        //    [StringLength(256, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        //    [Display(Name = "Role Name")]
        //    public string Name { get; set; }

        //}

        //public class UsersInRoleModel
        //{
        //    public string Id { get; set; }
        //    public List<string> EnrolledUsers { get; set; }
        //    public List<string> RemovedUsers { get; set; }
        //}


        //public class ModelFactory
        //{
        //    //Code removed for brevity

        //    public RoleReturnModel Create(IdentityRole appRole)
        //    {

        //        return new RoleReturnModel
        //        {
        //            //Url = _UrlHelper.Link("GetRoleById", new { id = appRole.Id }),
        //            Id = appRole.Id,
        //            Name = appRole.Name
        //        };
        //    }
        //}

        //public class RoleReturnModel
        //{
        //    public string Url { get; set; }
        //    public string Id { get; set; }
        //    public string Name { get; set; }
        //}
    }
}
