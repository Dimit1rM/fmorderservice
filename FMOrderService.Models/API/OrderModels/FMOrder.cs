﻿using FMOrderService.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Models.API.OrderModels
{
    [DataContract]
    public class FMOrder : DBOrder
    {
        public string OrderTime { get; set; }
        public string CreatedTime { get; set; }
        public string KitchenTime { get; set; }
        public string DriverTime { get; set; }

        public FMOrder() : base()
        {

        }
    }
}
