﻿using FMOrderService.DBModels;
using FMOrderService.Models.API.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Models.API.MonitoringModels
{
    public class OrderLogsViewModel
    {
        public FMOrder Order { get; set; }
    }
}
