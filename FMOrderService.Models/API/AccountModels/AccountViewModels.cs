﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Models.API.AccountModels
{
    // Models returned by AccountController actions.

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }
    }
}
