﻿using FMOrderService.API.Base;
using FMOrderService.Models.API.OrderModels;
using FMOrderService.Services.Interfaces;
using FMOrderService.WorkflowEngine;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FMOrderService.API.Controllers
{
    
    [RoutePrefix("api/Order")]
    public class OrderController : BaseApiController
    {
        [Dependency]
        public IOrderService OrderService { get; set; }

        // POST api/values
        [HttpPost]
        public IHttpActionResult Create(FMOrder order)
        {
            if (order == null)
            {
                return BadRequest();
            }
            var workflowEngine = new WorkflowEngine.WorkflowEngine(order);
            var result = workflowEngine.SaveOrderToDB();
            workflowEngine.Run();
            return Json(new { orderID = result.OrderID, status = result.Status, message = result.Message });
        }
    }
}
