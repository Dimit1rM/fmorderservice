﻿using FMOrderService.DB.DBContext;
using FMOrderService.Models.API.MonitoringModels;
using FMOrderService.Models.API.OrderModels;
using FMOrderService.Services.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMOrderService.API.Controllers
{
    public class MonitoringController : Controller
    {
        [Dependency]
        public IOrderService OrderService { get; set; }

        // GET: Monitoring
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OrdersLogs()
        {
            var model = new OrdersLogsViewModel();
            model.Orders = OrderService.Take(20);
            return View(model);
        }

        public ActionResult OrderLogs(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }
            var model = new OrderLogsViewModel();
            model.Order = OrderService.GetOrderByID(id);
            return View(model);
        }
    }
}