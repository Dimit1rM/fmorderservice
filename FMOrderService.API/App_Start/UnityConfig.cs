﻿using FMOrderService.Unity;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Unity.Mvc5;

namespace FMOrderService.API.App_Start
{
    public class UnityConfig
    {
        public static void RegisterComponents()
        {
            UnityContainer container = UnityConfiguration.GetUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
        
    }
}