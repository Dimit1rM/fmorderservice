﻿using FMOrderService.Automapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMOrderService.API.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            AutomapperConfiguration.Configure();
        }
    }
}