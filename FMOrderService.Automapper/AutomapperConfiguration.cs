﻿using AutoMapper;
using FMOrderService.DBModels;
using FMOrderService.Models.API.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Automapper
{
    public static class AutomapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<DBOrder, FMOrder>().ForMember(x => x.CreatedTime, opt => opt.ResolveUsing(src =>
                {
                    var dt = (DateTime)src.CreatedTimeUTC;
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
                }));

                //cfg.CreateMap<DBOrder, FMOrder>();
                cfg.CreateMap<FMOrder, DBOrder>().ForMember(x => x.CreatedTimeUTC, opt => opt.ResolveUsing(src =>
                    {
                        return DateTime.Parse(src.CreatedTime);
                    }));
                //TODO: Map the others here as follow:
                //cfg.CreateMap<DBOrder, FMOrder>().ForMember(); or other method
            });
        }
    }
}