﻿using FMOrderService.Automapper;
using FMOrderService.DB.DBContext;
using FMOrderService.DBModels;
using FMOrderService.LOGGER;
using FMOrderService.Models.API.OrderModels;
using FMOrderService.WorkflowEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = new Logger();
            //AutomapperConfiguration.Configure();
            //var context = new FMOrderServiceContext();
            //var context2 = new FMOrderServiceContext();
            //var order = context.Orders.GetByID("599bf367629a0522e859ba6e");
            //order.OrderType = 89;
            //var changedItem = context2.Orders.UpdateOne(order);
            logger.Debug("App Started");
            var order = new FMOrder();
            order.OrderType = 2;
            order.OrderTimeUTC = DateTime.UtcNow.AddHours(2);
            var workflowEngine = new WorkflowEngine(order);
            workflowEngine.Run();
        }
    }
}
