﻿using FMOrderService.Models.ExternalAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Interfaces
{
    public interface IBackendManager
    {
        Task<RestaurantDataModel> GetRestaurantDataAsync(RestaurantDataRequestModel model);
    }
}
