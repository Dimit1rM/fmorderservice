﻿using FMOrderService.Services.Classes;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Interfaces
{
    public interface IRestClient
    {
        RestStatusCodes StatusCode { get; set; }
        WebRequesterResponse ResponseModel { get; set; }
        //T Post<T>(string url, out RestStatusCodes status, object parameters = null);
        //T Get<T>(string url, NameValueCollection getParams = null);
        
        Task<T> PostAsync<T>(string url, object parameters = null);
        Task<T> GetAsync<T>(string url, NameValueCollection getParams = null);
    }
}
