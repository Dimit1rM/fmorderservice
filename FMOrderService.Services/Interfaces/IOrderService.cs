﻿using FMOrderService.DB.DBContext;
using FMOrderService.Models.API.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Interfaces
{
    public interface IOrderService
    {
        FMOrder GetOrderByID(string id);
        List<FMOrder> Take(int n);
    }
}
