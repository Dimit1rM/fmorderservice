﻿using FMOrderService.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FMOrderService.Services.Classes
{
    public class RestClient : IRestClient
    {
        public Cookie AuthCookie { get; set; }

        public WebRequesterResponse ResponseModel { get; set; }

        public RestStatusCodes StatusCode { get; set; }

        public async Task<T> PostAsync<T>(string url, object parameters = null)
        {
            var requester = new WebRequester();
            ResponseModel = await requester.MakeRequestAsync(url, parameters, WebRequestMethods.Http.Post, AuthCookie);
            if (ResponseModel == null)
            {
                StatusCode = RestStatusCodes.GENERAL_ERROR;
                return default(T);
            }
            else
            {
                try
                {
                    var streamString = ResponseModel.StreamReader.ReadToEnd();
                    //Log.InfoFormat("[RestClient] URL: {0} \n Params: {1} \n Response: {2}", url, JsonConvert.SerializeObject(parameters), streamString);
                    ResponseObject<T> result = JsonConvert.DeserializeObject<ResponseObject<T>>(streamString);
                    StatusCode = (RestStatusCodes)result.StatusCode;
                    if (result.StatusCode == (int)RestStatusCodes.SUCCESS) {
                        return (T)result.Payload;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                catch (Exception e)
                {
                    //logger.LogRestProblem(e, url, null, null);
                    //Log.InfoFormat("[RestClient] URL: {0} \n Params: {1} \n Response: {2}", url, JsonConvert.SerializeObject(parameters), e.Message + "\n" + e.StackTrace);
                    StatusCode = RestStatusCodes.GENERAL_ERROR;
                    return default(T);
                }
            }
        }
        
        public async Task<T> GetAsync<T>(string url, NameValueCollection getParams = null)
        {
            var preparedUri = getParams != null ? prepareUrl(url, getParams) : new Uri(url);

            var requester = new WebRequester();
            ResponseModel = await requester.MakeRequestAsync(preparedUri.ToString(), null, WebRequestMethods.Http.Get, AuthCookie);
            if (ResponseModel == null)
            {
                StatusCode = RestStatusCodes.GENERAL_ERROR;
                return default(T);
            }
            else
            {
                //var serializer = new JavaScriptSerializer();
                try
                {
                    var streamString = ResponseModel.StreamReader.ReadToEnd();
                    //Log.InfoFormat("[RestClient] URL: {0} \n Params: {1} \n Response: {2}", url, preparedUri, streamString);
                    ResponseObject<T> result = JsonConvert.DeserializeObject<ResponseObject<T>>(streamString);
                    StatusCode = (RestStatusCodes)result.StatusCode;
                    if (result.StatusCode == (int)RestStatusCodes.SUCCESS)
                    {
                        return (T)result.Payload;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                catch (Exception e)
                {
                    //Log.InfoFormat("[RestClient] URL: {0} \n Response: {1}", preparedUri.ToString(), e.Message + "\n" + e.StackTrace);
                    StatusCode = RestStatusCodes.GENERAL_ERROR;
                    return default(T);
                }
            }
        }

        private Uri prepareUrl(string url, NameValueCollection getParams)
        {
            getParams.Add("_time", DateTime.Now.Ticks.ToString());
            var q = HttpUtility.ParseQueryString(string.Empty);
            q.Add(getParams);
            var builder = new UriBuilder(url) { Query = q.ToString() };
            return builder.Uri;
        }
    }
}
