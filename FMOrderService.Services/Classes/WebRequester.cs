﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Classes
{
    public class WebRequester
    {
        public WebRequester()
        {
        }

        public async Task<WebRequesterResponse> MakeRequestAsync(String url, object requestParams, string method, Cookie authCookie = null)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = method;
            request.ContentType = "application/json";
            request.CookieContainer = new CookieContainer();
            if (authCookie != null)
            {
                request.CookieContainer.Add(authCookie);
            }

            if (method.Equals(WebRequestMethods.Http.Post))
            {
                StreamWriter sw = new StreamWriter(request.GetRequestStream());
                String postParamsString = JsonConvert.SerializeObject(requestParams);
                //Log.DebugFormat("URL: {0} DATA: {1}", url, postParamsString);
                sw.Write(postParamsString);
                sw.Close();
            }
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                //logger.LogRestProblem(e, url, null, null);
                return null;
            }

            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Unauthorized)
            {
                var result = new WebRequesterResponse()
                {
                    StreamReader = new StreamReader(response.GetResponseStream())
                };
                if (response.Headers.AllKeys.Contains("Set-Cookie"))
                {
                    result.AuthCookieHeader = response.Headers["Set-Cookie"];
                }
                return result;
            }
            else
            {
                //logger.LogRestProblem(url, (int)response.StatusCode, "WebRequester/MakeRequestAsync", null);
            }
            return null;
        }

        public WebRequesterResponse MakeRequest(String url, object requestParams, string method, Cookie authCookie = null)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = method;
            request.ContentType = "application/json";
            request.CookieContainer = new CookieContainer();
            if (authCookie != null)
            {
                request.CookieContainer.Add(authCookie);
            }

            if (method.Equals(WebRequestMethods.Http.Post))
            {
                StreamWriter sw = new StreamWriter(request.GetRequestStream());
                String postParamsString = JsonConvert.SerializeObject(requestParams);
                //Log.DebugFormat("URL: {0} DATA: {1}", url, postParamsString);
                sw.Write(postParamsString);
                sw.Close();
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Unauthorized)
            {
                var result = new WebRequesterResponse()
                {
                    StreamReader = new StreamReader(response.GetResponseStream())
                };
                if (response.Headers.AllKeys.Contains("Set-Cookie"))
                {
                    result.AuthCookieHeader = response.Headers["Set-Cookie"];
                }
                return result;
            }
            return null;
        }
    }

    public class WebRequesterResponse
    {
        public StreamReader StreamReader { get; set; }
        public string AuthCookieHeader { get; set; }
    }
}
