﻿using FMOrderService.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMOrderService.DB.DBContext;
using FMOrderService.Models.API.OrderModels;
using AutoMapper;
using FMOrderService.DBModels;

namespace FMOrderService.Services.Classes
{
    public class OrderService : IOrderService
    {
        private FMOrderServiceContext DBContext { get; set; }
        public OrderService()
        {
            this.DBContext = new FMOrderServiceContext();
        }


        public FMOrder GetOrderByID(string id)
        {
            return Mapper.Map<DBOrder, FMOrder>(DBContext.Orders.GetByID(id));
        }

        public List<FMOrder> Take(int n)
        {
            var dbOrders = this.DBContext.Orders.Take(n);
            var mapped = new List<FMOrder>();
            foreach (var order in dbOrders)
            {
                mapped.Add(Mapper.Map<FMOrder>(order));
            }
            return mapped;
        }
    }
}
