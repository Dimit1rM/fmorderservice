﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Classes
{
    public enum RestStatusCodes
    {
        SUCCESS = 0,

        BACKEND_OUT_OF_DATA = 6,

        UNAUTHORIZED = 1001,
        GENERAL_ERROR = 1002,
        INVALID_DATA = 1003,
        INVALID_TOKEN = 1004,
        EXPIRED_TOKEN = 1005,

        ERROR_GENERAL = 100,//2
        UNEXISTING_OBJECT = 101
    }
}
