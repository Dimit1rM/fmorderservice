﻿using FMOrderService.Models.ExternalAPI;
using FMOrderService.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using System.Configuration;
using FMOrderService.DBModels;

namespace FMOrderService.Services.Classes
{
    public class BackendManager : IBackendManager
    {
        [Dependency]
        protected IRestClient RestClient { get; set; }

        protected string BaseUrl = ConfigurationManager.AppSettings["BackendAPIUrl"];//http://fmdevapi.fast-menu.com
        public static readonly string RESTAURANT_DATA = "{0}/api/RestaurantData";
        public static readonly string NOTIFICATIONS_DATA = "{0}/api/NotificationsData";
        public static readonly string POS_DATA = "{0}/api/POSData";
        public static readonly string PRINTER_DATA = "{0}/api/PrinterData?restaurantId{1}";


        public static readonly string ITEMS_DATA = "{0}/api/ItemsData";
        

        //protected StatusCodes StatusCode;

        public BackendManager()
        {

        }

        public BackendManager(string baseUrl)
        {
            BaseUrl = baseUrl;
        }

        public async Task<RestaurantDataModel> GetRestaurantDataAsync(RestaurantDataRequestModel model)
        {
            return await RestClient.PostAsync<RestaurantDataModel>(string.Format(RESTAURANT_DATA, BaseUrl), model);
        }

        public async Task<NotificationsDataModel> GetNotificationsDataAsync(RestaurantDataRequestModel model)
        {
            return await RestClient.PostAsync<NotificationsDataModel>(string.Format(NOTIFICATIONS_DATA, BaseUrl), model);
        }

        public async Task<POSDataModel> GetPOSDataAsync(POSDataRequestModel model)
        {
            return await RestClient.PostAsync<POSDataModel>(string.Format(POS_DATA, BaseUrl), model);
        }

        public async Task<PrinterDataModel> GetPrinterDataAsync(int restaurantId)
        {
            return await RestClient.PostAsync<PrinterDataModel>(string.Format(PRINTER_DATA, BaseUrl, restaurantId), null);
        }

        public async Task<List<ItemModel>> GetPrinterDataAsync(ItemsDataRequestModel model)
        {
            return await RestClient.PostAsync<List<ItemModel>>(string.Format(ITEMS_DATA, BaseUrl), model);
        }


        
    }
}
