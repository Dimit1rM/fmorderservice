﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Services.Classes
{
    public class ResponseObject<T>
    {
        public T Payload { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
