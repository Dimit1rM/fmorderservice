﻿using FMorderService.FMConfigManager;
using FMOrderService.DB.DBContext;
using FMOrderService.WorkflowEngine.Models.ResponseModels;
using FMOrderService.WorkflowEngine.Models.Steps;
using FMOrderService.WorkflowEngine.Models.Steps.Interfaces;
using FMOrderService.WorkflowEngine.ServiceBusModels;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AutoMapper;
using FMOrderService.Models.API.OrderModels;
using FMOrderService.DBModels;

namespace FMOrderService.WorkflowEngine
{
    [DataContract]
    [KnownType(typeof(SendEmailStep))]
    [KnownType(typeof(SendToPOSStep))]
    public class WorkflowEngine
    {
        [DataMember]
        private List<IWorkflowStep> Steps { get; set; }
        [DataMember]
        public FMOrder Order { get; set; }

        public WorkflowEngine(FMOrder order)
        {
            this.Order = order;
            this.SetWorkflowSteps(order);
        }

        public SaveOrderResponse SaveOrderToDB()
        {
            if (!this.ValidateOrder())
            {
                return new SaveOrderResponse() { OrderID = (string)this.Order.GetOrderIDAsString(), Status = (int)OrderStatus.FailedToValidate, Message = OrderStatus.GetStatusText(OrderStatus.FailedToValidate) };
            }
            var response = new SaveOrderResponse();
            using (var dbContext = new FMOrderServiceContext())
            {
                int status;
                if (string.IsNullOrEmpty(this.Order.GetOrderIDAsString()))
                {
                    var savedOrder = dbContext.Orders.Add(Mapper.Map<FMOrder, DBOrder>(this.Order));
                    status = OrderStatus.AddedToDatabase;
                }
                else
                {
                    dbContext.Orders.UpdateOne(this.Order);
                    status = OrderStatus.SavedToDatabase;
                }


                response.OrderID = this.Order.GetOrderIDAsString();
                response.Status = status;
                response.Message = OrderStatus.GetStatusText(status);
            }
            return response;
        }

        public void Run()
        {
            if (!this.Steps.Any())
            {
                //this.Logger.Debug("There are no steps to follow.", this.Order.GetOrderIDAsString(), "");
                return;
            }
            this.SendMessageToServiceBus();
        }

        public void NextStep()
        {
            var currentStepID = this.Steps.FirstOrDefault(x => x.IsCurrent).ID;
            if (this.Steps.Any(x=>x.ID == currentStepID + 1))
            {
                Steps.ForEach(x => x.IsCurrent = false);
                Steps.First(x => x.ID == currentStepID + 1).IsCurrent = true;
                SendMessageToServiceBus();
            }
            else
            {
                this.LogOrderState("Order is completed");
                this.SaveOrderToDB();
            }
        }

        public bool NeedProcessing(Type stepType)
        {
            return this.Steps.First(x=>x.IsCurrent).GetType() == stepType;
        }

        public void LogOrderState(string message)
        {
            this.Order.Log(this.Order.OrderStatus, string.Format("[Step: {0}] - {1}", this.Steps.First(x=>x.IsCurrent).GetType().Name ,message));
        }

        #region PrivateMethods
        private void SendMessageToServiceBus()
        {
            //var connectionString = ConfigManager.ServiceBusConnectionStringForSending;
            var connectionString = "Endpoint=sb://devorderservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManagePolicy;SharedAccessKey=fwCI1RUN1dwJxgMaO5Ah4lZT0vAfOw3mILB/cNI1CgQ=;EntityPath=orderservicemain"; //"ConfigManager.ServiceBusConnectionStringForSending;

            //var topicName = "orderservicemain"; //ConfigManager.TopicName;
            //var namespaceManager = NamespaceManager.Create();
            //Configure all subsriptions
            //SubscriptionDescription notificationWorkerSubscription = namespaceManager.CreateSubscription(topicName, "NotificationWorker");

            ////Here is one more with filter
            ////var filter = new RuleDescription() { Filter = new SqlFilter("")};
            //SubscriptionDescription POSWOrkerSubscription = namespaceManager.CreateSubscription(topicName, "POSWOrker");


            var client = TopicClient.CreateFromConnectionString(connectionString);
            var message = new BrokeredMessage(new BusMessageModel() { WorkflowEngine = this });
            client.Send(message);
        }

        /// <summary>
        /// This method should create the steps based on the order's configurations
        /// </summary>
        /// <param name="order"></param>
        private void SetWorkflowSteps(FMOrder order)
        {
            this.Steps = new List<IWorkflowStep>();
            var stepIDCounter = 1;
            //Here you must have logic for every possible step. Consider using private methods to keep the code in this method cleaner
            if (true)
            {
                this.Steps.Add(new SendEmailStep() { ID = stepIDCounter });
                stepIDCounter++;
            }
            if (true)
            {
                this.Steps.Add(new SendToPOSStep() { ID = stepIDCounter });
                stepIDCounter++;
            }

            //set current step (the first one)
            this.Steps.First().IsCurrent = true;
        }

        private bool ValidateOrder()
        {
            //TODO: Validating... Consider using a model for the return type containing status and message.
            return true;
        }
        #endregion PrivateMethods
    }
}
