﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.WorkflowEngine.Models.ResponseModels
{
    public class SaveOrderResponse
    {
        public string OrderID { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
