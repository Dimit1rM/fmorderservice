﻿using FMOrderService.WorkflowEngine.Models.Steps.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.WorkflowEngine.Models.Steps
{
    [DataContract]
    public class SendToPOSStep : IWorkflowStep
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public bool IsCurrent { get; set; }
    }
}
