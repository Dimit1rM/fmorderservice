﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.WorkflowEngine.Models.Steps.Interfaces
{
    public interface IWorkflowStep
    {
        int ID { get; set; }
        bool IsCurrent { get; set; }
    }
}
