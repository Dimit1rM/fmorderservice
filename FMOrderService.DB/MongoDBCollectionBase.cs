﻿using FMOrderService.DB.DBContext;
using FMOrderService.DBModels;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.DB
{
    public class MongoDBCollectionBase<T> : IDisposable where T : BaseDBModel 
    {
        private IMongoDatabase Database { get; set; }
        private IMongoCollection<T> Collection { get; set; }

        public MongoDBCollectionBase(IMongoCollection<T> collection, IMongoDatabase database)
        {
            this.Collection = collection;
            this.Database = database;
        }
        
        public T Add(T newDocument)
        {
            newDocument.DateCreated = new BsonDateTime(DateTime.UtcNow);
            this.Collection.InsertOne(newDocument);
            return newDocument;
        }
        public List<T> AddMany(ICollection<T> newDocuments)
        {
            foreach (var document in newDocuments)
                document.DateCreated = new BsonDateTime(DateTime.UtcNow);

            this.Collection.InsertMany(newDocuments);
            return newDocuments.ToList();
        }
        public T GetByID(string ID)
        {
            var objectID = new ObjectId(ID);
            return this.Collection.AsQueryable().FirstOrDefault(x => x.ID == objectID);
        }
        public List<T> Take(int n)
        {
            return this.Collection.AsQueryable().Take(n).OrderByDescending(x=>x.DateCreated).ToList();
        }
        public T UpdateOne(T obj)
        {
            obj.DateUpdated = new BsonDateTime(DateTime.UtcNow);
            var filter = Builders<T>.Filter.Eq(s => s.ID, obj.ID);
            var result = this.Collection.ReplaceOne(filter, obj);
            return obj;
        }
        public T UpdateOne(string ID, T obj)
        {
            var objectID = new ObjectId(ID);
            obj.DateUpdated = new BsonDateTime(DateTime.UtcNow);
            var filter = Builders<T>.Filter.Eq(s => s.ID, objectID);
            var result = this.Collection.ReplaceOne(filter, obj);
            return obj;
        }

        public void Dispose()
        {
            Database = null;
            Collection = null;
        }
    }
}
