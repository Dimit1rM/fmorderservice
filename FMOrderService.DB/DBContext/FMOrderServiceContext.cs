﻿using FMorderService.FMConfigManager;
using FMOrderService.DBModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.DB.DBContext
{
    public class FMOrderServiceContext : IDisposable
    {
        private MongoClient client;
        private IMongoDatabase database;

        public FMOrderServiceContext()
        {
            MongoClientSettings settings = new MongoClientSettings();
            var credentials = MongoCredential.CreateCredential("FMOrderService_Dev", "OSAdmin", "Webteh1234");
            settings.Credentials = new[] { credentials };
            settings.Server = new MongoServerAddress("78.130.237.67", 2717); //this is by default

            //TODO: Must pass MongoSettings. Take them from the AppSettings
            this.client = new MongoClient(settings);
            this.database = this.client.GetDatabase("FMOrderService_Dev");

            //TODO: Set Mongo collections
            this.Orders = new MongoDBCollectionBase<DBOrder>(database.GetCollection<DBOrder>("Orders"), this.database);
        }
        //TODO: Mongo Collections
        public MongoDBCollectionBase<DBOrder> Orders { get; set; }

        public void Dispose()
        {
            this.client = null;
            this.database = null;

            //TODO: Dispode all collections
            this.Orders.Dispose();
        }
    }
}
