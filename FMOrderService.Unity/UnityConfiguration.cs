﻿using FMOrderService.LOGGER;
using FMOrderService.Services.Classes;
using FMOrderService.Services.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.Unity
{
    public class UnityConfiguration
    {
        public static UnityContainer GetUnityContainer()
        {
            var container = new UnityContainer();

            //Log
            container.RegisterType<ILogger, Logger>();
            //Services
            container.RegisterType<IOrderService, OrderService>();
            container.RegisterType<IBackendManager, BackendManager>();
            container.RegisterType<IRestClient, RestClient>();

            return container;
        }
    }
}
