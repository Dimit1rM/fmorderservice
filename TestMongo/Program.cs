﻿using FMOrderService.Models.API.OrderModels;
using FMOrderService.WorkflowEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMongo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create order
            var order = new FMOrder();
            order.OrderType = 2;
            order.OrderTimeUTC = DateTime.UtcNow.AddHours(2);
            var workflowEngine = new WorkflowEngine(order);
            workflowEngine.LogOrderState("Test");
            workflowEngine.SaveOrderToDB();
        }
    }
}
