﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace FMOrderService.LOGGER
{
    [DataContract]
    public class Logger : ILogger
    {
        //Should log every critical error in one file with date and also log for every different order in its separated file in another folder.
        ////public static ILog GetLogger([CallerMemberName]string fileName = "")
        ////{
        ////    return LogManager.GetLogger(fileName);
        ////}

        public void Debug(string message, [CallerFilePath]string fileName = "")
        {
            try
            {
                fileName = fileName.Split('\\').Last();
            }
            catch (Exception)
            {

            }  
            var log = LogManager.GetLogger(fileName);
            log.Debug(message);
        }

        public void Debug(string message, string orderID, [CallerMemberName]string fileName = "")
        {
            var log = LogManager.GetLogger("");
            log.Debug(String.Format("[OrderID: {0}] - {1}",orderID ,message));
        }
    }
}
