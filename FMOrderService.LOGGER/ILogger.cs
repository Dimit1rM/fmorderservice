﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FMOrderService.LOGGER
{
    public interface ILogger
    {
        void Debug(string message, [CallerFilePath]string fileName = "");
        void Debug(string message, string orderID, [CallerMemberName]string fileName = "");
      
    }
}
